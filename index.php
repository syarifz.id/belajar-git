<?php

    require('Animal.php');
    require('Frog.php');
    require('Ape.php');

    $sheep = new Animal("shaun");

    echo "Nama Hewan: ".$sheep->name; // "shaun"
    echo "<br>Jumlah Kaki:".$sheep->legs; // 4
    echo "<br>Apakah Cold Blooded? ".$sheep->cold_blooded; // "no"

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    $kodok = new Frog("buduk");
    echo "<br><br>Nama Hewan: ".$kodok->name; // "shaun"
    echo "<br>Jumlah Kaki:".$kodok->legs; // 4
    echo "<br>Apakah Cold Blooded? ".$kodok->cold_blooded; // "no"
    $kodok->jump() ; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "<br><br>Nama Hewan: ".$sungokong->name; // "shaun"
    echo "<br>Jumlah Kaki:".$sungokong->legs; // 4
    echo "<br>Apakah Cold Blooded? ".$sungokong->cold_blooded; // "no"
    $sungokong->yell(); // "Auooo"

    // edit git




?>